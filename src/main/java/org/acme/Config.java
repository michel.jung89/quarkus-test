package org.acme;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

@Dependent
public class Config {
  @Produces
  public HazelcastInstance hazelcastInstance() {
    return Hazelcast.newHazelcastInstance();
  }
}
